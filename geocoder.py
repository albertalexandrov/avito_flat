import abc
import requests


class AbstractGeocoder(abc.ABC):

    @abc.abstractmethod
    def get_coordinates(self, address): pass


class YandexGeocoder(AbstractGeocoder):

    __slots__ = 'address'

    geocoder_url = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode='

    def get_coordinates(self, address: str) -> (tuple, None):
        """ Метод, который определяет координаты объекта по его адресу

        Если количество найденных объектов found равно 1, то возвращаются координаты в формате (широта, долгота)
        В противном случае (не найдено ни одного объекта или объектов больше одного) возвращается None.
        Количество найденных объектов, отличное от 1, говорит о том, что необходимо уточнить адрес.

        :param address: адрес объекта
        :type: str

        :returns: кортеж (широта, долгота), если found == 1, и None, если found != 1
        """

        gc_response = requests.get(YandexGeocoder.geocoder_url + address).json()
        found = int(gc_response['response']['GeoObjectCollection']['metaDataProperty']
                               ['GeocoderResponseMetaData']['found'])

        if found == 1:
            coordinates_str = (gc_response['response']['GeoObjectCollection']['featureMember'][0]
                                          ['GeoObject']['Point']['pos'].split())
            return float(coordinates_str[1]), float(coordinates_str[0])

        return None


class GoogleGeocoder(AbstractGeocoder):
    pass
