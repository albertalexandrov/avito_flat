import json
from avito import Flat

flat = Flat().to_dict('https://www.avito.ru/ufa/kvartiry/studiya_35_m_56_et._1531290151')

if flat['success']:
    with open('flat.json', 'w', encoding='utf8') as file:
        json.dump(flat['data'], file, indent=4, ensure_ascii=False)
else:
    print(flat['error'])
