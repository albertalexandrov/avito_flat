from datetime import datetime, timedelta

import requests
from bs4 import BeautifulSoup
from dateutil.parser import parse as parse_datetime

from geocoder import YandexGeocoder


class Flat:

    __slots__ = 'soup', 'geocoder'

    months = {
        'января': 'january', 'февраля': 'february', 'марта': 'march', 'апреля': 'april', 'мая':
        'may', 'июня': 'june', 'июля': 'july', 'августа': 'august', 'сентября': 'september',
        'октября': 'october', 'ноября': 'november', 'декабря': 'december'}

    def __init__(self, geocoder=None):
        self.soup = None
        self.geocoder = geocoder if geocoder else YandexGeocoder()

    def to_dict(self, url: str) -> dict:
        """ Метод, преобразовывающий html-представление квартиры в dict-представление """

        request_response = requests.get(url)
        response = {'success': False}

        if 'block' in request_response.url:
            response['error'] = 'Похоже Авито нас заблокировал'
            return response
        elif request_response.status_code != 200:
            response['error'] = f'Код ошибки {request_response.status_code}'
            return response

        self.soup = BeautifulSoup(request_response.text, 'lxml')

        rooms, square, floor, floors = self.get_room_square_floor_floors()
        price_all = self.get_price_all()
        address = self.get_address()

        flat = {'ad_number': self.get_ad_number(url),
                'ad_url': url,
                'address': address,
                'rooms': rooms,
                'square': square,
                'floor': floor,
                'floors': floors,
                'price_all': price_all,
                'price_sq_meter': round(price_all / square),
                'publish_date': self.get_publish_date(),
                'photos': self.get_photos(),
                'seller': self.get_seller(),
                'coordinates': self.geocoder.get_coordinates(address),
                'object_type': self.get_object_type()}

        response['success'] = True
        response['data'] = flat

        return response

    @staticmethod
    def get_ad_number(url: str) -> int:
        """ Метод, возвращающий номер объявления """

        return int(url.split('_')[-1])

    def get_room_square_floor_floors(self) -> tuple:
        """ Метод, возвращающий количество комнат rooms, площадь квартиры square, этаж floor и этажность дома floors

        Сначала в объекте self.soup ищется тег span с классом title-info-title-text, в котором хранится
        заголовок объявления (пример: "Студия, 28.2 м², 7/19 эт.").  Видно, что заголовок содержит в себе данные
        о количестве комнат, площади квартиры, этаже и этажности.  Затем заголовок рабивается по символу ",",
        что дает нам список (пример: ['Студия', ' 28.2 м²', ' 7/19 эт.']).  Из полученного списка при
        помощи встроенных функций работы со строками удобно извлекать необходимые данные.

        :returns: кортеж (rooms, square, floor, floors)
        """

        raw_data = self.soup.find('span', 'title-info-title-text').text.split(',', 2)

        rooms = raw_data[0].split('-', 1)[0]
        rooms = 0 if rooms.lower() == 'студия' else int(rooms)

        square = float(raw_data[1].strip().split(' ', 1)[0])

        floors_raw = raw_data[2].strip().split(' ', 1)[0].split('/', 1)
        floor, floors = int(floors_raw[0]), int(floors_raw[1])

        return rooms, square, floor, floors

    def get_price_all(self) -> float:
        """ Метод, возвращающий полную стоимость квартиры """

        return round(float(self.soup.find('span', 'js-item-price').text.replace(' ', '')))

    def get_publish_date(self):
        """ Метод, возвращающий дату публикации объявления в UNIX-формате """

        raw_publish_date = str(self.soup.find('div', 'title-info-metadata-item').
                               text.strip().split(',', 1)[-1].replace('размещено', '').replace('в ', '').strip())

        if 'сегодня' in raw_publish_date:
            raw_publish_date = raw_publish_date.replace('сегодня', str(datetime.now().date()))
        elif 'вчера' in raw_publish_date:
            raw_publish_date = raw_publish_date.replace('вчера', str(datetime.now().date() - timedelta(1)))
        else:
            for month in Flat.months:
                if month in raw_publish_date:
                    raw_publish_date = raw_publish_date.replace(month, Flat.months[month])
                    break

        return round(parse_datetime(raw_publish_date).timestamp())

    def get_photos(self) -> dict:
        """ Метод, возвращающий информацию о фотографиях объявления

        :returns: словарь вида {'count': количество_фотографий, 'urls': [список_ссылок_на_фотографии]}
        """

        raw_photos = self.soup.find_all('div', 'gallery-img-frame js-gallery-img-frame')
        photos = {'count': len(raw_photos), 'urls': []}

        for photo in raw_photos:
            photos['urls'].append('https:' + photo.get('data-url'))

        return photos

    def get_seller(self) -> dict:
        """ Метод, возвращающий информацию о продавце

        :returns: словарь вида {'agency': False/True, 'name': имя_продавца, 'url': ссылка_на_продавца}
        """

        seller = {'agency': False}
        raw_seller = self.soup.find('div', 'seller-info-prop')

        link = raw_seller.find('a')
        seller['name'] = link.text.strip()
        seller['url'] = 'https://avito.ru' + link.get('href')

        if '<div>Агентство</div>' in str(raw_seller):
             seller['agency'] = True

        return seller

    def get_address(self) -> str:
        """ Метод, возвращающий адрес объекта """

        return self.soup.find_all('div', class_='seller-info-value')[-1].text.strip()

    def get_object_type(self) -> str:
        """ Метод, возвращающий тип объекта - первичка/вторичка """

        return self.soup.find('div', 'breadcrumbs').find_all('a')[-1].get('href').split('/')[-1]
